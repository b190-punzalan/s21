console.log("hellow world");


/*
try to store the following grades inside a variable and log the variable in the console
	98.5
	94.3
	89.2
	90.1
*/

let A = 98.5;
let B = 94.3;
let C = 89.2;
let D = 90.1;

let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);
//it is important that we store RELATED values inside an array so that its variable can live up to its description of its value
let computerBrands=["Lenovo", "Dell", "Asus", "HP", "MSI", "Acer", "CDR-King"];



/*
Array
	Array are used to store multiple related values in a single variable
	They are declared using square brackets([]) also known as "Array Literals"
	Arrays also provide access to a number of functions/methods that help in achieving tasks that we can perform on the elements inside the arrays

	Arrays are used to store numerous amounts of ata to manipulate in order perform methods

	Methods are used similar to functions associated with objects 

	Arrays are also referred to as objects which is another data type.
	The only difference between the tow is that arrays constant information in a form of list, but an object uses "properties" and "values" in its elements
*/

/*SYNTAX
	let/const arrayName = [elementA, elementB, elementC, ..., elementN];
*/

// this is possible but since we only provide list in arrays, this method of writing is not recommended
let mixedArr = [12, "Asus", null, undefined];
console.log(mixedArr);

// Reassigning of array values
console.log("Array before reassigning")
console.log(mixedArr);
// accessing the elements requires the arrayName + index enclosed in square brackets
mixedArr[0] = "Hello World";
console.log("Array after reassigning:");
console.log(mixedArr);

// SECTION - Reading from Arrays
/*
accessing array elements is one of the more common tasks that we do with an array; this can be done through the use of array indices
	- index - term used to declare the position of an element in an array.
	- in JS, the forst element is associated with number 0, incrementing as the number of the elements increase.
*/
console.log(grades);

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(computerBrands[6]);
console.log(computerBrands[7]);
// para ma-access, ilalagay mo lang inside [] yung index position ng gusto mong i-access
// undefined lilitaw or irereturn yung [7] kasi wala namang item for position 7

console.log(computerBrands.length);
// irereturn nito is 7 kasi 7 items although ang position starts with 0

// Common use for the .length property
if (computerBrands.length>5){
	console.log("We have too many suppliers. Please coordinate with the operations manager.")
}

let lastElementIndex= computerBrands.length-1;

console.log(computerBrands[lastElementIndex]);

// Section-Array Methods

// Methods are similar to functions, these array methods can be done in array and objects alone
// Mutator Methods
/*

*/

// SYNTAX:
//  array.Name.push()

let fruits = ["Apple", "Mango", "Rambutan", "Lanzones", "Durian"];
console.log(fruits);
// push()
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated Array frm Push Method");
console.log(fruits);

/*
fruits[6] ="Orange"
console.log(fruits);

the code above can also be written like this:
*/

fruits.push("Orange");
// it can also accept multiple argumatns like this below:
fruits.push("Guava", "Avocado");

console.log("Mutated Array from Push Method:");
console.log(fruits);

// pop() - remove an element from the end of an array
/*Syntax
array.Name.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Push Method");
console.log(fruits);

// kay pop walang choice kasi ung end of array lang ang istore niya

// unshift adds one or more elements to the beginning of an array and returns the new length of the array
/*
arrayName.unshift(element);
*/

// separating two unshifts will result into the second element being at the index 0 of the array 
fruits.unshift("Lime", "Banana");
console.log("Mutated Array from unshift Method;");
console.log(fruits);

// shift - removes an element from the start of the array 
// Syntax
// arrayName.shift();
let fruitRemoved = fruits.shift();
console.log("Mutated Array from shift Method;");
console.log(fruits);

fruits.shift();
console.log("Mutated Array from shift Method;");
console.log(fruits);

// splice - simultaneously removes and adds elements from specified index number

/*Syntax
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

fruits.splice(1,2, "Lime", "Cherry");
console.log("Mutated Array from splice Method;");
console.log(fruits);

// sort - rearranges elements in alphanumeric order
/*Syntax
	arrayName.sort();
*/
fruits.sort();
console.log("Mutated Array from sort Method;");
console.log(fruits);

// reverse - reverses the order of the array
fruits.reverse();
console.log("Mutated Array from reverse Method;");
console.log(fruits);

// Non-Mutator Methods
// these are functions that do not modify the original array. These methods do not manipulate the elements inside the elements inside the array even if they are performing tasks such as returning elements from an array and combining them

// indexOf() - returns the index of the first matching element found in an array
// the search process will start from the first element down to the last
// Syntax
// 	arrayName.indexOf(searchValue);
	// seacrchValue=yung element na hinahanap mo. make sure lang na you have the right element. pag wala naman sa array yung element na hinahanap mo, return niya ay -1 kasi non-exstent
let countries = ["PH", "RUS", "CH", "JPN", "KOR", "AUS", "CAN", "PH"];

let firstIndex=countries.indexOf("PH");
console.log("Result of indexOf: " + firstIndex);

// lastIndexOf() - same lang ng indexOf pero this starts search process from the last element down to the first

let lastIndex=countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: " + lastIndex);

// slice - copies a  part of the array and returns a new array
/*Syntax
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingElement);
		element and not index so wag malilito
*/
let slicedArrayA = countries.slice(2);
console.log("Result of Slice: " + slicedArrayA);
console.log(countries);
// yung countries naman hindi nagbago kaya same pa rin

// parang splice di pero si splice 

let slicedArrayB = countries.slice(2, 4);
console.log("Result of Slice: " + slicedArrayB);
console.log(countries);
// so ito, ang irereturn ay yung index 2 up to the 4th element which is JPN

let slicedArrayC = countries.slice(-3);
console.log("Result of Slice: " + slicedArrayC);
console.log(countries);
// pag negative, sa end siya magstart ng search niya pabaligtad pero ung order nila sa console ay same sa order nila sa original array

// toString() - returns a new array as a string separated by commas
/*Syntax
	arrayName.toString();
*/

let stringArray=countries.toString();
console.log("Result of toString: ");
console.log(stringArray);

// concat() - used to combine two arrays and returns the combined result
/*
Syntax:
	arrayA.concat(arrayB);
*/
let tasksA=["drink HTML", "eat Javascript"];
let tasksB=["inhale CSS", "breathe SASS"];
let tasksC=["get GIT", "be node"];

let tasks=tasksA.concat(tasksB);
console.log("Results of concat:");
console.log(tasks);

// combining multiple arrays
let allTasks=tasksA.concat(tasksB, tasksC);
console.log("Results of concat: ");
console.log(allTasks);

// join() - returns an array as a string separated by specified string separators(space, comma, dash)
/*
Syntax
	arrayName.join('stringSeparator');
*/

let users=["John", "Jane", "Joe", "Jobert", "Julius"];
console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));

// Iteration methods
/*
-Iteration methods are loops designed to perform repititive tasks on arrays
-useful for manipulating array data resulting in complex tasks
*/


// forEach
/*
	-similar to a for loop that loops through all elements
	- varaiable names for arrays are usually written in plural form of the data stored in an array
	- common practice to use the singular form of the array content for parameter names used in array loops
	Syntax:
		arrayName.forEach(function(individualElement){
			statement/s
		})
*/
allTasks.forEach(function(task){
	console.log(task);
})

// forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length>10){
		// we stored the filtered elements inside another variable to avoid confusion should we need the original array intact
		filteredTasks.push(task);
	}
})
console.log("Result of forEach ");
console.log(filteredTasks);

// map - iterates on each element AND returns a new array with different values depending on the result of the function's operations
// dito di required magkaron tayo ng another array
//this is useful for performing tasks where mutating/changing the elements are required
/*
syntax
	let/const resultArray=arrayName.map(function(individualElement){
		return statement
	})
*/
// unlike forEach, return statement is needed in map method to create a value that is stored in another array
let numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
	return number*number;
})
console.log("Result of map: ");
console.log(numbersMap);

// every() - checks if ALL elements pass the given condition, pag kahit may isa lang na di makasatisfy, it will return false

/*
Syntax:
	let/const result = arrayName.every(function(individualElement){
		return expression/condition
	})
*/

let allValid=numbers.every(function(number){
	return (number<3);
})
console.log("Result of every: ");
console.log(allValid);

// some() - checks if at least one element in the array satisfy the condition. pag kahit isa pumasa, returns true. if walang tumama, false
// Syntax
/*let/const resultName = arrayName.some(function(individualElement){
	return expression/condition
})

*/

let someValid = numbers.some(function(numers){
	return(numbers<2);
})
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);


// filter - returns a new array that contains copies of the elements which meet the given condition
// pag walang nagmamatch sa condition, magreturn siya empty[]
// useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteratin methods such as forEach + if statement + push

// Syntax
/*
	let/const resultName = arrayName(function(individualElement){
		return expression/condition
	})
*/
let filterValid=numbers.filter(function(number){
	return(number<3);
})
console.log("Result of filter:");
console.log(filterValid);
console.log(numbers);

let parts = ["Mouse", "Keyboard", "Unit", "Monitor"];
console.log(parts);

// reduce();
/*
evaluates elements from left to right and returns/reduces the array into a single value
*/

/*Syntax
	let/const resultName = arrayName.reduce(function(accumulator, currentValue){
		return expression/operation
	})

	- accumulator - stores the result for every itertion
	-currentValue-is the next/current element in the array that is evaluated in each iterationS
*/

// using array of numbers
let iteration= 0;
let reducedArray = numbers.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x+y;
})
console.log(reducedArray);

// using array of strings
// when used in string, it returns the concat strongs of the array

let reducedStringArray = parts.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x+y;
})
console.log(reducedStringArray);

// includes method 
let filteredParts = parts.filter(function(part){
		return part.toLowerCase().includes("a");
});
console.log(filteredParts);