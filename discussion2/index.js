console.log("hello world");

let cities = ["Tokyo", "Manila", "Seoul", "Jakarta", "Sim"];
console.log(cities);
// to check the number of elements inside the array:
console.log(cities.length);
// to check the last index of the array:
console.log(cities.length-1);
// to check the last element through the index:
console.log(cities[cities.length-1]);

// in cases of blank arrays, 
blankArr=[];
console.log(blankArr);
console.log(blankArr.length);
// would return 0 kasi wala naman element sa array
// mag-undefine naman siya pag hinanap mo ung nth element:
console.log(blankArr[blankArr.length]);


console.log(cities);
cities.length--;
console.log(cities);
//decrement operator can be used in the .length property to delete the last element of the array

let lakersLegends = ["Kobe", "Shaq","Magic", "Kareem", "LeBron"];
console.log(lakersLegends);
lakersLegends.length++;
console.log(lakersLegends);

lakersLegends[5] = "Pau Gasol";
console.log(lakersLegends);

// kung gusto mong magdagdag, pwedeng mag .length ka na lang to directly add elements at the end of the array:
lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);

let numArray = [5, 12, 30, 46, 40];

for (let index=0; index<numArray.length; index++){
	if(numArray[index]%5===0){
		console.log(numArray[index] + " is dividsible by 5");
	} else{
		console.log(numArray[index] + " is not divisible by 5");
	}
}

// para maprint naman sila ng pabaligtad na order:
for (let index = numArray.length-1; index>=0; index--){
	console.log(numArray[index]);
}

// multidimensional arrays
/*
- are useful for storing complex data structures
- a practical applic of this is to help visualize/create real world objects
- though useful in a number of cases, creating complex arrays is not always recommended
*/
let chessBoard = [
["a1", "b1", "c1", "d1", "e1"],
["a2", "b2", "c2", "d2", "e2"],
["a3", "b3", "c3", "d3", "e3"],
["a4", "b4", "c4", "d4", "e4"],
["a5", "b5", "c5", "d5", "e5"],
];
console.log(chessBoard);
// accessing a multi-dmensional array:
console.log(chessBoard[1][4]);
// kaya nag lumitaw dito ay e2

console.log("Pawn moves to " +chessBoard[2][3]);
